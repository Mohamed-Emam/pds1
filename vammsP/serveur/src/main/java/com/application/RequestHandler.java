
package com.application;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import com.model.*;



import com.dao.*;
import com.entities.*;
import com.connectionpool.*;
import com.serialization.*;


public class RequestHandler implements Runnable {
	private final DataInputStream is; 
    private final DataOutputStream os; 
    private final Socket sck;
    private final Connection cn;
    private Model model;
    
    public RequestHandler(Socket sck, Connection cn) throws IOException {
		super();
		this.cn=cn;
		this.sck = sck;
		this.is = new DataInputStream(sck.getInputStream());
		this.os = new DataOutputStream(sck.getOutputStream());
		model = new Model(cn);
	}
    
    public void run() {
		System.err.println("Lancement du traitement de la connexion cliente");

	    boolean closeConnexion = false;
	    //do this only when connexion is true
	    System.out.println(!sck.isClosed());
	    do{
			try {
				
				
				String inputFile = is.readUTF();
				if(inputFile.startsWith("it's time to close the socket"))
					break;
				System.out.println("Flux JSON côté serveur "+inputFile);
				SensorDAOImpl sdaoi = new SensorDAOImpl();
	            Request inputRequest = new Request();
				inputRequest = SerializationDeserialization.deserialization((String) inputFile);
				Request outputRequest = new Request();
				outputRequest.setSuccess(Boolean.FALSE);
				
				Statement sta = cn.createStatement();
				switch(inputRequest.getRequest()) {
				
				case "select-sensors" : 
					System.out.println("I take the case: select-sensors");
					
					
					sta = cn.createStatement();
					String condition = inputRequest.getConditionLabelValue();
					ResultSet ri=null;
					if(condition.contains("-"))
						ri = sta.executeQuery("select * from Sensor");
					
					else
						ri = sta.executeQuery("select *,type from sensor,sensor_type where sensor.fk_type = sensor_type.id ");
					int cont=0;
					while(ri.next())
						cont++;
					os.writeInt(cont);
					
					int k=0;
					
					ri.close();
					sta.close();
					
					
					Statement stati = cn.createStatement();
					
					ResultSet re = null;
					if(condition.contains("-"))
						re = stati.executeQuery("select * from Sensor");
					else
						ri = sta.executeQuery("select *,type from sensor,sensor_type where sensor.fk_type = sensor_type.id ");
					
					
					while(re.next()){
						Request requestSensor = new Request();
						requestSensor.setRequest("sensor");
						Sensor sen = new Sensor();
						sen.setIp_address(re.getString("ip_address"));
						sen.setState(re.getString("state"));
						sen.setInstallation_date(re.getDate("installation_date").toString());
						sen.setInstalled(re.getBoolean("installed"));
						sen.setScope(re.getInt("scope"));
						
						sen.setMac_address(re.getString("mac_address"));
						
						sen.setFk_room( Integer.toString(re.getInt("fk_room")));
						sen.setFk_type(Integer.toString(re.getInt("fk_type")));
						
						sen.setLocation(re.getString("location"));
						
						sen.setSensibility(re.getString("sensibility"));
						
						sen.setPrice(re.getDouble("price"));
						sen.setX(re.getInt("x"));
						
						sen.setY(re.getInt("y"));
						
						sen.setMark(re.getString("mark"));
						
						sen.setType(re.getString("type"));
						

						
						
						
						requestSensor.setObject(sen);
						System.out.println(k++);
						requestSensor.setSuccess(true);
						String req =SerializationDeserialization.serialization(requestSensor);
						os.writeUTF(req);
					}
					
		
					outputRequest.setObject("");
					outputRequest.setRequest("");
					outputRequest.setSuccess(true);
					
					break;
				
				
				
					case "inser sensor-map" : Sensor sensor = new Sensor();
											System.out.println("I take the case: insert sensor-map");
											sensor = SerializationDeserialization.deserialization(inputRequest.getObject(), sensor);
											sdaoi.insert(sensor, cn);
											outputRequest.setSuccess(Boolean.TRUE);
											
					break;
					
					case "select x and y" : 
						System.out.println("I take the case: select x and y ");
						
						sta = cn.createStatement();
						ResultSet result1 = sta.executeQuery("select * from sensor ");					
						int cont1=0;
						while(result1.next())
							cont1++;
						
						System.out.println(cont1);
						os.writeInt(cont1);
					
						result1.close();
						sta.close();
						
						
						Statement st2 = cn.createStatement();
						
						ResultSet re1 = st2.executeQuery("select * from sensor");
						
						while(re1.next()){
							
							Request requeteclient = new Request();
							Sensor s = new Sensor();
							
							s.setX(re1.getInt("x"));
							s.setY(re1.getInt("y"));
							s.setState(re1.getBoolean("state"));
							requeteclient.setObject(s);
							requeteclient.setSuccess(true);
	 
							String req =SerializationDeserialization.serialization(requeteclient);
							os.writeUTF(req);
						}
						
						outputRequest.setObject("");
						
						break;
						
					
					case "delete-sensor" : System.out.println("I take the case: delete-sensor");
					Sensor sensorDelete = new Sensor();
					String label = "mac_address";
					sensorDelete = SerializationDeserialization.deserialization(inputRequest.getObject(), sensorDelete);
										sdaoi.delete(cn, label, sensorDelete.getMac_address());
										outputRequest.setSuccess(Boolean.TRUE);
					break;
					
					
					case "select-sensorPrice" : 
						System.out.println("I take the case: select-sensorScope");
						String ty = inputRequest.getLabelValue();
						System.out.println(ty);
						sta = cn.createStatement();
						ResultSet result2 = sta.executeQuery("select * from sensor inner join sensor_type on sensor.fk_type = sensor_type.id where sensor_type.type = '"+ty+"' ");					
						int cont2=0;
						while(result2.next())
							cont2++;
						
						System.out.println(cont2);
						os.writeInt(cont2);
					
						result2.close();
						sta.close();
						
						
						Statement st3 = cn.createStatement();
						
						ResultSet re2 = st3.executeQuery("select * from sensor inner join sensor_type on sensor.fk_type = sensor_type.id where sensor_type.type = '"+ty+"' ");
						
						while(re2.next()){
							
							Request requeteclient = new Request();
							Sensor s = new Sensor();
							
							s.setPrice(re2.getDouble("price"));
							s.setScope(re2.getInt("scope"));
							requeteclient.setObject(s);
							requeteclient.setSuccess(true);
	 
							String req =SerializationDeserialization.serialization(requeteclient);
							os.writeUTF(req);
						}
						
						outputRequest.setObject("");
						
						break;
					
					
		    
				}
				
				String outputFile = SerializationDeserialization.serialization(outputRequest);
				os.writeUTF(outputFile);
			}catch(IOException e) {
				e.printStackTrace();
			}catch(DAOException e) {
				System.out.println("fail");
			}catch(SQLException e) {
				System.out.println("fail during the statement");
				Request fail = new Request();
				fail.setSuccess(Boolean.FALSE);
			}
		
	    }
	    while(!sck.isClosed());
	    DataSource.freeConnection(cn);
	    System.out.println("Client is disconnected");
    }
  
}
