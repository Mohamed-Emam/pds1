package com.application;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.util.Properties;

import com.connectionpool.Config;
import com.connectionpool.DataSource;

public class ServerAcceptor {
	
	
	private static Connection connection;
	
		   private int port;
		   private ServerSocket server = null;
		   private boolean isRunning = true;
	   public ServerAcceptor(){
		  port= Config.getPort();
	      try {
	         server = new ServerSocket(port);
	      } catch (UnknownHostException e) {
	         e.printStackTrace();
	      } catch (IOException e) {
	         e.printStackTrace();
	      }
	   }
	   
	   
	
	   public void open(){
	      
	      
	      Thread t = new Thread(new Runnable(){
	         public void run(){
	            while(isRunning == true){
	                Socket client = null;
	                 Boolean state ;
	               try { 
	               	 client = server.accept();
	               	  sos = new DataOutputStream(client.getOutputStream());
	               	 if(DataSource.poolUsedSize()== Config.getNb_max()) {
		                	//  client.close();
		                	 
		                	  System.out.println("Nombre maximal de connexion atteinte ");
		                	
		                	state = false ;
		                	sos.writeBoolean(state);
		                	
		                	Thread.sleep(5000);
		                	 // continue ;
		                  }
		                  else {
		                	  state = true ;
		                	  sos.writeBoolean(state);
		                	  
;
		                  }

		                  // Socket client = server.accept();
		                   System.out.println("Client is connected !");
	                  
		              connection = null ;
	            	  connection = DataSource.getConnection();
	            	  System.out.println(connection.getSchema());
	            	  System.out.println(connection.getMetaData().getURL());
	            	   System.out.println("Client is connected !");
	                  
	                  // Waiting socket from client 
	                 
	                  //One thread by client 
	                 
	                  Thread t = new Thread(new RequestHandler(client, connection));
	                  t.start();
	                  
	               } catch (Exception e) {
	            	   if (e.getMessage().equals("wait")) {
	   					continue;
	   				}
	                  e.printStackTrace();
	               }
	            }
	            
	            try {
	               server.close();
	            } catch (IOException e) {
	               e.printStackTrace();
	               server = null;
	            }
	         }
	      });
	      
	      t.start();
	   }
	   
	   public void close(){
	      isRunning = false;
	   }   
}

