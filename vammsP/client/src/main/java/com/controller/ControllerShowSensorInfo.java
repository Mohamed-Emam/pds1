package com.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JOptionPane;

import com.entities.Request;
import com.entities.Sensor;
import com.serialization.SerializationDeserialization;
import com.view.ShowSensorInfoView;

public class ControllerShowSensorInfo  {
	
	private ShowSensorInfoView pssiv ;
	String mac_address ;
	//String type ;
	Integer scope ;
	String mark ;
	Double price ;
	
	
	public ControllerShowSensorInfo(String mac_address, int scope,String mark,double price) {
		super();
		this.mac_address= mac_address ;
		this.scope= scope;
		this.mark = mark ;
		this.price= price ;
		this.requestDeleteSensor();
		
	}


	
	public void requestDeleteSensor() {
		SocketRequestor.closeSocket();
		SocketRequestor.createSocket();
		
		
		try {
			Request rq = new Request();
			rq.setRequest("delete-sensor");
			Sensor sn = new Sensor();
			sn.setMac_address(mac_address);
			sn.setScope(scope);
			sn.setMark(mark);
			sn.setPrice(price);
			rq.setObject(sn);
			
			String req = SerializationDeserialization.serialization(rq);
			SocketRequestor.getDos().writeUTF(req);
			String responseString = SocketRequestor.getDis().readUTF();
			Request requestResponse = SerializationDeserialization.deserialization(responseString);

			if(requestResponse.getSuccess())
				JOptionPane.showMessageDialog(null,"Succès","Info request",JOptionPane.DEFAULT_OPTION);
			
			else 
				JOptionPane.showMessageDialog(null,"Echec","Info request",JOptionPane.ERROR_MESSAGE);
			
		}catch (IOException e1)
		{
			
		}
		
		
		
		
		
	}

}
