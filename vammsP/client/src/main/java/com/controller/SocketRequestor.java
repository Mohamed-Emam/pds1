package com.controller;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import javax.swing.JOptionPane;

import com.application.Main;



public class SocketRequestor {
	static Socket sck = null;
	static DataOutputStream dos;
	static DataInputStream dis;
	
	public static void createSocket() {
		try {
			
			sck = new Socket(ConfigClient.getAddress(), ConfigClient.getPort());
			dos = new DataOutputStream (sck.getOutputStream());
			dis = new DataInputStream (sck.getInputStream());	


			Boolean error ;
			error = dis.readBoolean();
			if (error == false ) {
				System.out.println("Nombre de connexion maximal atteint");
				 Main.f.getCd().show(Main.f.getContentPane(),"login");
				 JOptionPane.showMessageDialog(null, "Impossible de se connecter au serveur", "Erreur 401", JOptionPane.ERROR_MESSAGE);
				 sck.close();
			
			
		} catch (IOException e) {
			System.out.println("Fail during socket instance");
			 Main.f.getCd().show(Main.f.getContentPane(),"login");
			 JOptionPane.showMessageDialog(null, "Impossible to connect to server", "Error 401", JOptionPane.ERROR_MESSAGE);
		}
			
	}

	public static Socket getSck() {
		return sck;
	}

	public static DataOutputStream getDos() {
		return dos;
	}

	public static DataInputStream getDis() {
		return dis;
	}
	
	public static void closeSocket() {
		if(SocketRequestor.getSck()!=null) {
			try {
				dos.writeUTF("it's time to close the socket");
				sck.close();
				System.out.println("Client socket is closed");
			} catch (IOException e) {
				System.out.println("Impossible to close socket");
				e.printStackTrace();
			}
		}
	}

}
