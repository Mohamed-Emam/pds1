package com.controller;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JOptionPane;

import com.view.*;

public class ControllerMapView implements MouseListener,ActionListener{
	
	private MapView mp;
	private PAppli pa;
	private MapView refresh;
	public static String room ;
	
	
	public ControllerMapView(Fenetre f) {
		this.mp = f.getAp().getPu().getMap();
		this.pa = f.getAp();
		pa.getPu().getMap().addMouseListener(this);
		
		
		
	}
	
	
	 // Location mouseClicked
	   private boolean location(Point mouse, int x,int y, int w,int h) {
		   Rectangle rec = new Rectangle(x,y,w,h);
		 
		   if(rec.contains(mouse))
			   return true;
		   else
			   return false ;
	   }
	   
	
	
	@Override
	public void mouseClicked(MouseEvent e) {
		/*if(selectInfoSensor(e)) {
			return ;
		}*/
		
		Point mouse = e.getPoint();
		
		// Open a JOptionPane
		if(location(mouse,1175, 100, 175, 415)) {
			JOptionPane.showMessageDialog(null,"Impossible de placer un capteur ici","Info capteurs",JOptionPane.ERROR_MESSAGE);
		}
		
		// Open the Frame AddSensorview
		if(location(mouse,415, 380, 375, 250)) {
			
		 AddSensorView asv = new AddSensorView(e.getX(),e.getY());
		asv.setVisible(true);
		room = "Salle de soin";
		
		}

		if(location(mouse,795, 515, 450, 115)) {
			JOptionPane.showMessageDialog(null,"Impossible de placer un capteur ici","Info capteurs",JOptionPane.ERROR_MESSAGE);
		
		room = "Chambre";
		
		}

		if(location(mouse,1040, 70, 130, 180)) {
			
		 AddSensorView asv = new AddSensorView(e.getX(),e.getY());
		asv.setVisible(true);
		room = "Accueil";
		}
		
		if(location(mouse,1040, 255, 130, 115)) {
			
			 AddSensorView asv = new AddSensorView(e.getX(),e.getY());
			asv.setVisible(true);
			room = "Centre opérationnel";
			}
			
		if(location(mouse,795,400, 375, 115)) {
			
			 AddSensorView asv = new AddSensorView(e.getX(),e.getY());
			asv.setVisible(true);
			room = "Salle activité";
			}
			
		
		if(location(mouse,415,85,370,270)) {
			
			 AddSensorView asv = new AddSensorView(e.getX(),e.getY());
			asv.setVisible(true);
			room = "Animation collective";
			}
			
		if(location(mouse,785,85,250,190)) {
			
			 AddSensorView asv = new AddSensorView(e.getX(),e.getY());
			asv.setVisible(true);
			room = "Animation collective";
			}
		
		
	}  
	

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
		public boolean selectInfoSensor(MouseEvent e) {
		SocketRequestor.closeSocket();
		SocketRequestor.createSocket();
		 
		boolean b = false ;
		try {
			Request rq1 = new Request();
			rq1.setRequest("select-sensors");
			rq1.setConditionLabelValue("");
			String requestString1 = SerializationDeserialization.serialization(rq1);
			SocketRequestor.getDos().writeUTF(requestString1);
			
			int cptXY = 0 ;
			cptXY = SocketRequestor.getDis().readInt();
			for (int i = 0; i < cptXY ;i++) {
				String inputfile = SocketRequestor.getDis().readUTF();
				
				Request getSensor = new Request();
				getSensor = SerializationDeserialization.deserialization(inputfile);
				
				Sensor sensor = new Sensor();
				sensor = SerializationDeserialization.deserialization(getSensor.getObject(),sensor);
				int fk_type = Integer.parseInt(sensor.getFk_type());
				int fk_room = Integer.parseInt(sensor.getFk_room());
				int scope = sensor.getScope();
				String mac_address = sensor.getMac_address() ;
				String mark = sensor.getMark();
				String type = sensor.getType();
	
				int x = e.getX();
				int y = e.getY();
				if (x-10 < sensor.getX() && sensor.getX()< x+10) {
					
				
					
					if(y-10 < sensor.getY() && sensor.getY()< y+10) {
						b = true ;
						ssiv = new ShowSensorInfoView();
						//String address = sensor.getMac_address();
						
						ssiv.getJtMac_address().setText(mac_address);
						//ssiv.getJtState().setText(sensor.getState());
						ssiv.getJtType().setText(type);	
						ssiv.getJtPrice().setText(String.valueOf(price));
						ssiv.getJtScope().setText(String.valueOf(scope));
						ssiv.getJtMark().setText(mark);
						ssiv.setVisible(true);
					
					
				}
			}
				
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		return b ;
	}


	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		mp.repaint();
		// TODO Auto-generated method stub
		
	}



	
	

	
	

}
