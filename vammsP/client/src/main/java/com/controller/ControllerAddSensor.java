package com.controller;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JOptionPane;

import com.view.AddSensorView;

import com.entities.*;
import com.serialization.SerializationDeserialization;


public class ControllerAddSensor {
	
	public static int[] tabX=  new int [300];
	public static int[] tabY= new int [300] ;
	public static boolean[] tabstate=  new boolean [300];
	
	private String type;
	private String ip_address;
	private String mac_address;
	private double price ;
	private int scope;
	private String fk_type;
	private String fk_room;
	private String mark ;
	private int x;
	private int y;

	public ControllerAddSensor(String type, String ip_address, String mac_address, double price, int scope, int x, int y, String mark) {
		super();
		this.type = type;
		this.ip_address = ip_address;
		this.mac_address = mac_address;
		this.price = price;
		this.scope = scope;
		this.x = x;
		this.y =y;
		this.mark = mark ;
		this.requestAddSensorForMap();
	}
	
	
		
	
	public void requestAddSensorForMap() {
		// TODO Auto-generated method stub
		Sensor sen = new Sensor();
		Request rq = new Request();
		sen.setFk_room(ControllerMapView.room);
		sen.setFk_type((String)AddSensorView.comboBox.getSelectedItem());
		sen.setMac_address(mac_address);
		sen.setPrice(price);
		sen.setScope(scope);
		sen.setMark(mark);
		sen.setX(x);
		sen.setY(y);
		
		rq.setObject(sen);
		rq.setRequest("inser sensor-map");
		String requestString = SerializationDeserialization.serialization(rq);
		JOptionPane.showMessageDialog(null,requestString,"Flux Json",JOptionPane.DEFAULT_OPTION);
		try {
			SocketRequestor.getDos().writeUTF(requestString);
			String responseString = SocketRequestor.getDis().readUTF();
			Request requestResponse = SerializationDeserialization.deserialization(responseString);
		/*	if(requestResponse.getSuccess())
				JOptionPane.showMessageDialog(null,"Succès","Info request",JOptionPane.DEFAULT_OPTION);
			
			else 
				JOptionPane.showMessageDialog(null,"Echec","Info request",JOptionPane.ERROR_MESSAGE);*/
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		
		
		 SocketRequestor.closeSocket();
		 SocketRequestor.createSocket();
		 
		 
		// getXY();

		}

		public static void getXY(){
		try {
			Request rq1 = new Request();
			rq1.setRequest("select x and y");
			String requestString1 = SerializationDeserialization.serialization(rq1);
			SocketRequestor.getDos().writeUTF(requestString1);
			
			int cptXY = 0 ;
			cptXY = SocketRequestor.getDis().readInt();
			for (int i = 0; i < cptXY ;i++) {
				String inputfile = SocketRequestor.getDis().readUTF();
				
				Request getXY = new Request();
				getXY = SerializationDeserialization.deserialization(inputfile);
				
				Sensor sensor = new Sensor();
				sensor = SerializationDeserialization.deserialization(getXY.getObject(),sensor);
				tabX[i] = sensor.getX();
				tabY[i] = sensor.getY();
				tabstate[i] = sensor.getState();
				
				
				
			}
			
			
		} catch (IOException e) {
			
			// TODO: handle exception
		}
		
		
	}
}
