package com.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

public class PMenu extends JPanel {
	
	private JButton map, config_sensor;
	private JButton add_sensor,deconnexion;
	
	public PMenu() {
		super();
		map = new JButton("Map");
		config_sensor = new JButton("Capteurs");
		//indicator = new JButton("Indicateurs");
		//add_sensor = new JButton("Ajouter capteurs");
		//need = new JButton("Besoin équipement");
		deconnexion = new JButton ("Déconnexion");
		this.setLayout(new GridLayout(1,6));
		this.add(map);
		//this.add(add_sensor);
		this.add(config_sensor);
		//this.add(indicator);
		//this.add(need);
		this.add(deconnexion);
		this.setPreferredSize(new Dimension(150,150));
		this.setBackground(Color.BLUE);
		
	}

	public PMenu(int i) {
		super();
		map = new JButton("Map");
		config_sensor = new JButton("Configurer capteurs");
		//indicator = new JButton("Indicateurs");
		//add_sensor = new JButton("Ajouter capteurs");
	//	need = new JButton("Besoin équipement");
		deconnexion = new JButton ("Déconnexion");
		this.setLayout(new GridLayout(1,6));
		this.add(map);
		
		//this.add(indicator);
		//this.add(need);
		this.add(deconnexion);
		this.setPreferredSize(new Dimension(150,150));
		this.setBackground(Color.BLUE);
	}

	public JButton getMap() {
		return map;
	}

	public JButton getConfig_sensor() {
		return config_sensor;
	}

	/*public JButton getIndicator() {
		return indicator;
	}

	public JButton getAdd_sensor() {
		return add_sensor;
	}

	public JButton getNeed() {
		return need;
	}*/

	public JButton getDeconnexion() {
		return deconnexion;
	}

	

	
}
