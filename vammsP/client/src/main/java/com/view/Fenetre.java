package com.view;

import java.awt.CardLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

import com.controller.SocketRequestor;



	public class Fenetre extends JFrame {
		private CardLayout cd;
		private PLogin pl;
		private PAppli ap;
		private PMenu pm;
		private PAppli financialAp;
		public Fenetre() {
			super("VAMMS");
			cd = new CardLayout();
			this.setLayout(cd);
			pl = new PLogin();
			ap = new PAppli(this);
			financialAp = new PAppli(0, this);
			// add to the ihm
			this.getContentPane().add("login",pl);
			this.getContentPane().add("ap",ap);
			this.getContentPane().add("FinancialApp",financialAp);
			cd.show(this.getContentPane(), "login");
			
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setLocation(550,150);
			this.setSize(800, 800);
			this.setVisible(true);
			this.addWindowListener(new WindowListener() {
				
				
				@Override
				public void windowOpened(WindowEvent e) {}
				@Override
				public void windowIconified(WindowEvent e) {}
				@Override
				public void windowDeiconified(WindowEvent e) {}
				@Override
				public void windowDeactivated(WindowEvent e) {}
				@Override
				public void windowClosed(WindowEvent e) {}
				@Override
				public void windowActivated(WindowEvent e) {}
				
			
				@Override
				public void windowClosing(WindowEvent e) {
					
						SocketRequestor.closeSocket();
				}	
				
			});
		}
		public CardLayout getCd() {
			return cd;
		}
		public PLogin getPl() {
			return pl;
		}
		public PAppli getAp() {
			return ap;
		}

	}


