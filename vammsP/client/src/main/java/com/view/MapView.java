package com.view;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

import com.controller.ControllerAddSensor;

public class MapView extends JPanel{
	
	   private static final int RECT_X = 500;
	   private static final int RECT_Y = RECT_X;
	   private static final int RECT_WIDTH = 300;
	   private static final int RECT_HEIGHT = RECT_WIDTH;
	   public static double position_x, position_y ;
	   JButton refresh ;
	   
	   
	  /* public MapView() {
		
		refresh = new JButton ("REFRESH");
		ActionListener al = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ControllerAddSensor.getXY();
				// TODO Auto-generated method stub
				repaint();
			}
		};
		refresh.addActionListener(al);
		add(refresh); 
		
	}*/

		public void autoRefresh() {
		   Thread t = new Thread(new Runnable(){
		         public void run(){
		        	
		        	 
		        	    SocketRequestor.closeSocket();
		 				SocketRequestor.createSocket();
		 				ControllerAddSensor.getXY();
		        		repaint();
		        		
		        		
		        		
		         }
		        	 
		         
		   });
		   t.start();
	   }
	   
	   public void autoRefreshAtTheBeginning() {
		   Thread t = new Thread(new Runnable(){
		         public void run(){
		        	
		        	    SocketRequestor.createSocket();
		        	    ControllerAddSensor.getXY();
		        	    repaint();
		        		SocketRequestor.closeSocket();
		 				
		         }
		        	 
		         
		   });
		   t.start();
	   }
	   


	@Override
	   protected void paintComponent(Graphics g) {
	      super.paintComponent(g);
	      // draw the rectangle here
	      
	      g.drawRect(1175, 100, 175, 415); // Pièce en travaux
	      g.drawRect(415, 380, 375, 250); // Salle de soin
	      g.drawRect(795, 515, 450, 115); // Chambre
	      g.drawRect(1035, 70, 140, 180); // Accueil
	      g.drawRect(1035, 255, 140, 115); // Centre
	      g.drawRect(795,400, 375, 115); //Salle d'activité
	      g.drawRect(415,85,370,270); // Animation collective
	      g.drawRect(785,85,250,190); // Animation collective
	      
	   
		
		// Map
		try {
			plan = Images.MAP_IMAGE.getImage(); 
			//BufferedImage img = ImageIO.read(new File("src/main/resources/Plan2.png"));
			g.drawImage(plan, 400, 60, 1000, 600, this);

				
		} catch (Exception e) {
			// TODO: handle exception
		}
	     //Logo brightness sensors
		
		
		
		// Sensor status and draw sensor
		for (int i=0 ; i< 300 ; i++) {
			int x =(int) ControllerAddSensor.tabX[i];
			int y=(int) ControllerAddSensor.tabY[i];
			boolean state = (boolean) ControllerAddSensor.tabstate[i];
			
			if(state == true) {
				g.setColor(Color.GRAY);
				g.fillOval(x, y, 10, 10);
			}
			else {
				g.setColor(Color.RED);
				g.fillOval(x, y, 10, 10);
				
			}
			}
		
  }
	   

	   @Override
	   public Dimension getPreferredSize() {
	      // so that our GUI is big enough
	      return new Dimension(RECT_WIDTH + 2 * RECT_X, RECT_HEIGHT + 2 * RECT_Y);
	   }

	   // create the GUI 
	   private static void createAndShowGui() {
	      MapView mainPanel = new MapView();
	      JFrame frame = new JFrame("Map");
	      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	      frame.getContentPane().add(mainPanel);
	      frame.pack();
	      frame.setLocationByPlatform(true);
	      frame.setVisible(true);
	  }
	   
			
	public static int getRectX() {
		return RECT_X;
	}
	public static int getRectY() {
		return RECT_Y;
	}
	public static int getRectWidth() {
		return RECT_WIDTH;
	}
	public static int getRectHeight() {
		return RECT_HEIGHT;
	}
	
	public static void main(String[] args) {
	      SwingUtilities.invokeLater(new Runnable() {
	         public void run() {
	            createAndShowGui();
	           
	           
	         }
	      });
		  
	  }
}


