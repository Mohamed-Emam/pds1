package com.view;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.controller.ControllerAddSensor;
import com.controller.ControllerShowSensorInfo;

public class ShowSensorInfoView extends JFrame {
	
	// Just to start this frame alone
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShowSensorInfoView frame = new ShowSensorInfoView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	JTextField jtMac_address;
	//JTextField jtState;
	JTextField jtType;
	JTextField jtPrice;
	JTextField jtScope;
	JTextField jtMark ;
	
	JButton delete ;
	MapView mainPanel = new MapView();
	
	public ShowSensorInfoView(){
		
		setSize(800,800);
		
		JLabel  jlMac_address =  new JLabel("Adresse Mac :");
		//JLabel jlState= new JLabel("State :");
		JLabel jlType = new JLabel("Type : ");
		JLabel jlPrice= new JLabel("Price:");
		JLabel jlScope = new JLabel("Scope :");
		JLabel jlMark  = new JLabel("Brand:");
		
		 jtMac_address = new JTextField("");
		// jtState = new JTextField("");
		 jtType = new JTextField("");
		 jtPrice = new JTextField("");
		 jtScope = new JTextField("");
		 jtMark = new JTextField("");
		 
		 delete = new JButton("Delete");
		 
		
		JPanel  jpShowSensorInfoView = new JPanel();
		
		jpShowSensorInfoView.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		//JLabel
		c.gridx = 0 ;
		c.gridy = 0 ;
		jpShowSensorInfoView.add(jlMac_address,c);
		
		//c.gridx = 1 ;
		//c.gridy = 0 ;
		//jpShowSensorInfoView.add(jlState,c);
		
		c.gridx = 2 ;
		c.gridy = 0 ;
		jpShowSensorInfoView.add(jlType,c);
		
		c.gridx = 3 ;
		c.gridy = 0 ;
		jpShowSensorInfoView.add(jlPrice,c);
		
		c.gridx = 4 ;
		c.gridy = 0 ;
		jpShowSensorInfoView.add(jlScope,c);
		
		c.gridx = 5 ;
		c.gridy = 0 ;
		c.insets = new Insets(30,30,20,20);
		jpShowSensorInfoView.add(jlMark,c);
		
		
		//JTextField
		c.gridx = 0 ;
		c.gridy = 1 ;
		jtMac_address.setPreferredSize(new Dimension(100,25));
		jpShowSensorInfoView.add(jtMac_address,c);
		
		//c.gridx = 1 ;
		//c.gridy = 1 ;
		//jtState.setPreferredSize(new Dimension(100,25));
		//jpShowSensorInfoView.add(jtState,c);
		
		c.gridx = 2 ;
		c.gridy = 1 ;
		jtType.setPreferredSize(new Dimension(100,25));
		jpShowSensorInfoView.add(jtType,c);
		
		c.gridx = 3 ;
		c.gridy = 1 ;
		jtPrice.setPreferredSize(new Dimension(100,25));
		jpShowSensorInfoView.add(jtPrice,c);
		
		c.gridx = 4 ;
		c.gridy = 1 ;
		jtScope.setPreferredSize(new Dimension(100,25));
		jpShowSensorInfoView.add(jtScope,c);
		
		c.gridx = 5 ;
		c.gridy = 1 ;
		jtMark.setPreferredSize(new Dimension(100,25));
		c.insets = new Insets(30,30,20,20);
		jpShowSensorInfoView.add(jtMark,c);
		
		c.gridx = 1 ;
		c.gridy = 2 ;
		jpShowSensorInfoView.add(delete,c);
		
		
		this.getContentPane().add(jpShowSensorInfoView);
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		delete.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				new ControllerShowSensorInfo(jtMac_address.getText(),Integer.parseInt(jtScope.getText()),jtMark.getText(),Double.parseDouble(jtPrice.getText()));
				mainPanel.autoRefresh();
			}
		});
	}
	public JButton getDelete() {
		return delete;
	}
	public void setDelete(JButton delete) {
		this.delete = delete;
	}
	public JTextField getJtMac_address() {
		return jtMac_address;
	}
	public void setJtMac_address(JTextField jtMac_address) {
		this.jtMac_address = jtMac_address;
	}
	/*public JTextField getJtState() {
		return jtState;
	}
	public void setJtState(JTextField jtState) {
		this.jtState = jtState;
	}*/
	public JTextField getJtType() {
		return jtType;
	}
	public void setJtType(JTextField jtType) {
		this.jtType = jtType;
	}
	public JTextField getJtPrice() {
		return jtPrice;
	}
	public void setJtPrice(JTextField jtPrice) {
		this.jtPrice = jtPrice;
	}
	public JTextField getJtScope() {
		return jtScope;
	}
	public void setJtScope(JTextField jtScope) {
		this.jtScope = jtScope;
	}
	public JTextField getJtMark() {
		return jtMark;
	}
	public void setJtMark(JTextField jtMark) {
		this.jtMark = jtMark;
	}

	
	
	
	
	
	
	

}
