package com.view;

import java.awt.CardLayout;
import java.awt.Color;
import javax.swing.JPanel;

public class PUseCase extends JPanel {

	
	
	private PAppli pa;
	private CardLayout cl;
	private MapView map;
	
	
	public PUseCase() {
		super();
		
		cl = new CardLayout();
		this.setLayout(cl);
		map = new MapView();
		this.add("Map",map);
		
						
	}
	public void setCard(String name) {
		cl.show(this, name);
	}



	public  MapView getMap() {
		return map;
	}


	
	public CardLayout getCl() {
		return cl;
	}
	
}