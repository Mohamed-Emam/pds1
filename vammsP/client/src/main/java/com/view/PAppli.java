package  com.view;
import java.awt.BorderLayout;

import javax.swing.JPanel;

import com.controller.*;

public class PAppli extends JPanel {
	private PMenu pm;
	private PUseCase pu;
	ControllerPMenu cm;
	
	private BorderLayout bl;
	
	public PAppli(Fenetre f) {
		super();
		pu = new PUseCase();
		pm = new PMenu();
		ControllerPMenu cm = new ControllerPMenu(pm, this,f);
		bl = new BorderLayout();
		this.setLayout(bl);
		this.add(pm,BorderLayout.SOUTH);
		this.add(pu,BorderLayout.CENTER);
		
		
		
	}

	public PAppli(int i, Fenetre f) {
		super();
		pu = new PUseCase();
		pm = new PMenu(0);
		ControllerPMenu cm = new ControllerPMenu(pm, this,f);
		bl = new BorderLayout();
		this.setLayout(bl);
		this.add(pm,BorderLayout.SOUTH);
		this.add(pu,BorderLayout.CENTER);
		
		
	}

	public PMenu getPm() {
		return pm;
	}

	public PUseCase getPu() {
		return pu;
	}

	public BorderLayout getBl() {
		return bl;
	}

}
