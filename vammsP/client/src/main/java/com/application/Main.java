package com.application;

import  com.view.Fenetre;
import com.controller.ControllerMapView;
import com.controller.FrameController;


public class Main {
	
	public static Fenetre f= new Fenetre();

	public static void main(String[] args) {
		new FrameController(f);
		
		new ControllerMapView(f);

		MapView mv = new MapView();
		mv.autoRefreshAtTheBeginning();
		
	}

}
