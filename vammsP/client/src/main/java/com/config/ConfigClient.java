package com.config;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class ConfigClient {
	private static ConfigClient config = new ConfigClient();
	private static  String  address;//DONE
	private static int port;
	
	
	
	private ConfigClient() {
		Properties p = new Properties();
		InputStream file = null;
        try {
			
			
        	file = getClass().getResourceAsStream("/configclient.properties");
			// load the file properties
			p.load(file);
			
			// recover data from properties
			address  = p.getProperty("address");
			 port = Integer.parseInt(p.getProperty("port"));
			
		} 
        catch (IOException ex) {
        	
        }
        
		finally {
			if (file != null) {
				try {
					file.close();
				} catch (IOException e) {
					
				}
			}
		}
		
        
	}
	
	public static String getAddress() {
		return address;
	}
		
	public static ConfigClient getConfig() {
		if(config==null) {
			config=new ConfigClient();
		}
		return config;
	}

	public static int getPort() {
		return port;
	}

	
}


