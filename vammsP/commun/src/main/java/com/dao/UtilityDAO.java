package com.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class UtilityDAO {
	
    private UtilityDAO() {
    }

   
    public static void silentClosure( ResultSet resultSet ) {
        if ( resultSet != null ) {
            try {
                resultSet.close();
            } catch ( SQLException e ) {
                System.out.println( "Failed to close resultset : " + e.getMessage() );
            }
        }
    }


    public static void silentClosure( Statement statement ) {
        if ( statement != null ) {
            try {
                statement.close();
            } catch ( SQLException e ) {
                System.out.println( "Failed to close Statement : " + e.getMessage() );
            }
        }
    }

    public static void silentClosures( ResultSet resultSet, Statement statement ) {
    	silentClosure( resultSet );
    	silentClosure( statement );
        
    }

   
    public static PreparedStatement preparedStatementInit( Connection connection, String sql, boolean returnGeneratedKeys, Object... objets ) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement( sql, returnGeneratedKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS );
        for ( int i = 0; i < objets.length; i++ ) {
        	System.out.println(i+" : "+objets[i]);
            preparedStatement.setObject( i +1, objets[i] );
        }
        return preparedStatement;
    }
}
