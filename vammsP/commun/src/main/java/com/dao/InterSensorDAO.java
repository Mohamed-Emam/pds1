package com.dao;

import java.sql.Connection;
import java.sql.ResultSet;

import com.entities.*;

   public interface InterSensorDAO{
	
	void insert( Sensor sensor, Connection cn ) throws DAOException;
	

    ResultSet select( String sql ,Connection cn ) throws DAOException;
    
    void delete(Connection cn,String label, String labelValue) throws DAOException;
    
    void update(Connection cn,String label,String labelValue,String condition, String conditionValue ) throws DAOException;
}
