package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import com.entities.Sensor;

public class SensorDAOImpl implements InterSensorDAO {

	private static final Connection Connection = null;
	@Override
	public void insert(Sensor sensor, Connection cn) throws DAOException {
		int succes = 0;
		
		try {
			Statement st = cn.createStatement();
			ResultSet rt = st.executeQuery("SELECT id FROM sensor_type where type = '" + sensor.getFk_type() + "'");
			rt.next();
			int fk_type = rt.getInt("id");
			rt.close();
			st.close();
			
		String sql = "INSERT INTO Sensor (mac_address, price,installed, scope, fk_type,x,y,mark) VALUES (?, ?,true,?,?,?,?,?)";
		PreparedStatement preparedstatement = cn.prepareStatement(sql);
		preparedstatement.setObject(1, sensor.getMac_address());
		preparedstatement.setObject(2, sensor.getPrice());
		preparedstatement.setObject(3, sensor.getScope());
		preparedstatement.setObject(4, fk_type);
		preparedstatement.setObject(5,sensor.getX());
		preparedstatement.setObject(6, sensor.getY());
		preparedstatement.setObject(7, sensor.getMark());
		
		System.out.println(preparedstatement);
		succes = preparedstatement.executeUpdate();
		
		preparedstatement.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ResultSet select(String sql, Connection cn) throws DAOException {
		// TODO Auto-generated method stubi
		return null;
	}

	@Override
	public void delete(Connection cn, String label, String labelValue) throws DAOException {
		try {
			Statement st = cn.createStatement();
			ResultSet rt = st.executeQuery("DELETE FROM Sensor where "+label+"='"+labelValue+"'");
			
			rt.close();
			st.close(); 
					
		} catch (SQLException e) {
			
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Connection cn, String label, String labelValue, String condition, String conditionValue)
			throws DAOException {
		// TODO Auto-generated method stub

	}

	public boolean update(Sensor sensor, Connection cn) {
		try {
		
		String sql = "update Sensor set "
						+ "ip_address = '"+sensor.getIp_address()+"',"
						+ "state = '"+ sensor.getState()+"',"
						+ "location= '"+sensor.getLocation()+"',"
						+ "installation_date= '"+sensor.getInstallation_date()+"',"
						+ "installed = "+sensor.isInstalled()+","
						+ "sensibility = '"+sensor.getSensibility()+"'"
						+ " where mac_address = '"+sensor.getMac_address()+"'"; 
		Statement st = cn.createStatement();
		int bool = st.executeUpdate(sql);
		if(bool!=0)
			return true; 
		
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return false;
		}
	public static void main(String[] args) {
		
		Sensor sensor= new Sensor();
		SensorDAOImpl name = new SensorDAOImpl();
		
	}
}

