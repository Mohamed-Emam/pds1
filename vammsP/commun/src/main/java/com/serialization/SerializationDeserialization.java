package com.serialization;
import java.sql.ResultSet;


import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import com.entities.Request;


public class SerializationDeserialization {
	
		static Gson gson = new Gson();
		
		public static String serialization(Request request) {
			
				String result = gson.toJson(request);
				System.out.println(result);
				System.out.println(request.getClass());
				return  result;
		}
		
		public static Request deserialization(String json) {
			Request result = new Request();
			result = gson.fromJson(json, result.getClass());
			System.out.println(gson.toJson(result.getObject()));
			return result;
		}
		
		public static <T> T deserialization(Object map, T object) {
			if (map.getClass()==LinkedTreeMap.class) {
				T object1 = object;
				object1 = (T) gson.fromJson(gson.toJson(map), object1.getClass());
				return object1;
			}
			return null;
		}
		
		
	}



