package com.enumeration;

import java.net.URL;
import java.awt.Image;
import javax.swing.ImageIcon;

public enum Images {
	
	MAP_IMAGE("Plan2.png");
	
	private URL url;


	Images(String imageName)
	{
		url = getClass().getClassLoader().getResource("images/" + imageName);
	}

	public ImageIcon getIcon()
	{
		return new ImageIcon(url);
	}

	public Image getImage()
	{
		return getIcon().getImage();
	}


}
