package com.entities;

import java.sql.Timestamp;

public class Sensor {

	
	private long id;
	private String type;
	private String sensibility;
	private String location;
	private String state;
	private String ip_address;
	private String mac_address;
	private double price;
	private String installation_date;
	private boolean installed;
	private int scope;
	private String fk_type;
	private String fk_room;
	private String mark ;
	private int x,y ;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSensibility() {
		return sensibility;
	}
	public void setSensibility(String string) {
		this.sensibility = string;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
	public String getMac_address() {
		return mac_address;
	}
	public void setMac_address(String mac_address) {
		this.mac_address = mac_address;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getInstallation_date() {
		return installation_date;
	}
	public void setInstallation_date(String installation_date) {
		this.installation_date = installation_date;
	}
	public boolean isInstalled() {
		return installed;
	}
	public void setInstalled(boolean installed) {
		this.installed = installed;
	}
	public int getScope() {
		return scope;
	}
	public void setScope(int scope) {
		this.scope = scope;
	}
	public String getFk_type() {
		return fk_type;
	}
	public void setFk_type(String fk_type) {
		this.fk_type = fk_type;
	}
	public String getFk_room() {
		return fk_room;
	}
	public void setFk_room(String fk_room) {
		this.fk_room = fk_room;
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
}
	