package com.entities;

import java.io.Serializable;

public class Room {
	private Serializable id;
	private String floor;
	private String type;
	private String name;
	private int m2;
	private String wing;
	public Serializable getId() {
		return id;
	}
	public String getFloor() {
		return floor;
	}
	public String getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	public int getM2() {
		return m2;
	}
	public String getWing() {
		return wing;
	}
	public void setId(Serializable id) {
		this.id = id;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setM2(int m2) {
		this.m2 = m2;
	}
	public void setWing(String wing) {
		this.wing = wing;
	}
}	